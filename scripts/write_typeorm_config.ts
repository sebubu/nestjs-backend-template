import fs = require('fs');
const YAML = require('json-to-pretty-yaml');

require('dotenv').config();


function dbConfig() {
    return {
        type: 'postgres',
  
        host: process.env.DB_HOST,
        port: parseInt(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
  
        entities: ['**/*.entity{.ts,.js}', 'node_modules/nestjs-admin/**/*.entity.js'],
  
        migrationsTableName: 'migration',
  
        migrations: ['src/migration/*.ts'],
  
        cli: {
          migrationsDir: 'src/migration',
        }
    }
}

const config = dbConfig();
fs.writeFileSync('ormconfig.json',
 JSON.stringify(config, null, 2)
);
