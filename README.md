# Nestjs

- Start app: `npm run start:dev`
- Generate migrations: `npm run typeorm:migration:generate -- %migration_name%`
- Generate emptry migrations: `npm run typeorm:migration:create -- %migration_name%`
- Run migrations: `npm run typeorm:migration:run`
- Create fixtures `npm run typeorm:fixtures ./fixtures`




