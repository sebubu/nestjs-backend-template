import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UsersService } from '../users/users.service';

@Injectable()
export class JwtAdminStrategy extends PassportStrategy(Strategy, 'jwtadmin') {
  constructor(
    private readonly configService: ConfigService,
    private readonly usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('JWT_SECRET'),
    });
  }

  async validate(payload: any) {
    if (payload['type'] !== 'access') {
      throw new UnauthorizedException();
    }
    const user = await this.usersService.findOne({id: payload.id, isAdmin: true, isActive: true});
    if (user) {
      return user;
    }
    throw new UnauthorizedException();
  }
}