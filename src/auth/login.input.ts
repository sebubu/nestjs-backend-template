import { InputType, Field } from "type-graphql";
import { IsEmail, IsString, MinLength } from "class-validator";

@InputType()
export class LoginInput {
    @Field()
    @IsEmail()
    email: string;

    @Field()
    @IsString()
    @MinLength(4)
    password: string
}