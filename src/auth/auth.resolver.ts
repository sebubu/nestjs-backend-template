import { Resolver, Query, Args, Mutation } from "@nestjs/graphql";

import { UseGuards, ValidationPipe } from "@nestjs/common";

import { ValidationError } from "apollo-server-core";
import { UsersService } from "./users/users.service";
import { UserAlreadyExistsPipe } from "./user-already-exists.pipe";
import { SignupInput } from "./signup.input";
import { LoginInput } from "./login.input";
import { AuthService } from "./auth.service";
import { LoginDto } from "./login.dto";
import { GqlRefreshJwtGuard } from "./gql-guards/gql-refresh-jwt.guard";
import { CurrentUser } from "./gql-guards/gql-current-user.decorator";
import { User } from "./users/user.entity";
import { IsEmail } from "class-validator";




@Resolver()
export class AuthResolver {
    constructor(
        private readonly userService: UsersService,
        private readonly authService: AuthService
    ) { }

    @Mutation(returns => LoginDto)
    async signup(@Args('signupInput', ValidationPipe, UserAlreadyExistsPipe) signupInput: SignupInput) {
        const user = await this.userService.create(signupInput.name, signupInput.email, signupInput.password);
        return await this.authService.login(user);
    }

    @Query(returns => Boolean)
    async userExists(@Args('email') email: string) {
        const user = await this.userService.findOne({email: email})
        if (user) {
            return true;
        } else {
            return false;
        }
    }

    @Mutation(returns => LoginDto)
    async login(@Args('login', ValidationPipe) login: LoginInput) {
        const user = await this.authService.validateUser(login.email, login.password)
        if (user) {
            return await this.authService.login(user);
        }
        throw new ValidationError('Email or password invalid');
    }

    @Mutation(returns => LoginDto)
    @UseGuards(GqlRefreshJwtGuard)
    async refreshAccessToken(@CurrentUser() user: User) {
        const accessToken = await this.authService.refreshAccessToken(user);
        const result = {
            refreshToken: null,
            ...accessToken
        };
        return result;
    }

}