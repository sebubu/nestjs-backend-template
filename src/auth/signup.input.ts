import { InputType, Field } from "type-graphql";
import { IsEmail, IsString, MinLength } from "class-validator";

@InputType()
export class SignupInput {
    
    @Field()
    @IsEmail()
    email: string;

    @Field()
    @IsString()
    @MinLength(4)
    password: string

    @Field()
    @IsString()
    @MinLength(3)
    name: string

}