import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from './users/users.module';
import { LocalStrategy } from './strategies/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RefreshjwtStrategy } from './strategies/refresh-jwt.strategy';
import { JwtAdminStrategy } from './strategies/jwt-admin.strategy';
import { AuthResolver } from './auth.resolver';

@Module({
  providers: [
    AuthService, 
    LocalStrategy,
    JwtStrategy,
    JwtAdminStrategy,
    RefreshjwtStrategy,
    AuthResolver
  ],
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        const options: JwtModuleOptions = {
          secret: configService.get('JWT_SECRET'),
        };
        return options;
      },
      inject: [ConfigService],
    }),
  ],
  controllers: []
})
export class AuthModule { }
