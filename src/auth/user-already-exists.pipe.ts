import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { UsersService } from './users/users.service';
import { ValidationError } from 'apollo-server-core';


@Injectable()
export class UserAlreadyExistsPipe implements PipeTransform<any> {

    constructor(private userService: UsersService) {

    }
  async transform(value: any, { metatype }: ArgumentMetadata) {
    const user = await this.userService.findOne({email: value.email})
    if (user) {
        throw new ValidationError('User with this email address already exists.');
    }
    return value;
  }
}