import { Resolver, Query } from "@nestjs/graphql";
import { UsersService } from "./users.service";
import { UserDto } from "./user.dto";
import { UseGuards } from "@nestjs/common";
import { GqlJwtGuard } from "../gql-guards/gql-jwt.guard";
import { CurrentUser } from "../gql-guards/gql-current-user.decorator";
import { User } from "./user.entity";
import { GqlJwtAdminGuard } from "../gql-guards/gql-jwt-admin.guard";
import { AuthService } from "../auth.service";



@Resolver(of => UserDto)
export class UserResolver {
    constructor(
        private readonly userService: UsersService
    ) { }

    @Query(returns => [UserDto])
    @UseGuards(GqlJwtAdminGuard)
    async users() {
        return await this.userService.find({});
    }

    @Query(returns => UserDto)
    @UseGuards(GqlJwtGuard)
    profile(@CurrentUser() user: User) {
        return this.userService.findOne({ id: user.id });
    }


}