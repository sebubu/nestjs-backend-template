import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';
import { hash, compare } from 'bcrypt';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 200 })
  name: string;

  @Column({ length: 200})
  email: string;

  @Column({ length: 200})
  hashedPassword: string;

  @Column()
  isActive: boolean;

  @Column()
  isAdmin: boolean;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
    createDateTime: Date;

  public async setPassword(password: string) {
    this.hashedPassword = await hash(password, 10);
  }

  public async checkPassword(password: string): Promise<boolean> {
    return compare(password, this.hashedPassword);
  }
}