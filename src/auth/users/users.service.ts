import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  private readonly users: User[];

  constructor(@InjectRepository(User)
  private readonly userRepository: Repository<User>) {
  }

  async findOne(data: Partial<User>): Promise<User | undefined> {
      return this.userRepository.findOne(data);
  }

  async find(data: Partial<User>): Promise<Array<User>> {
    return this.userRepository.find(data);
  }

  async create(name: string, email: string, password: string) {
    const user = new User();
    user.name = name;
    user.email = email;
    await user.setPassword(password);
    user.isActive = true;
    user.isAdmin = false;
    return this.userRepository.save(user)
  }
}