import { Field, ObjectType } from 'type-graphql';


@ObjectType()
export class UserDto {
    @Field({nullable: true})
    id: string;

    @Field()
    name: string;

    @Field()
    email: string;

    @Field()
    isActive: boolean;

    @Field()
    isAdmin: boolean;

    @Field()
    createDateTime: Date;
}