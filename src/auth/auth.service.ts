import { Injectable } from '@nestjs/common';
import { UsersService } from './users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from './users/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService
  ) { }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findOne({
      email: email,
      isActive: true
    });
    if (user && user.checkPassword(password)) {
      const { hashedPassword: hashed_password, ...result } = user;
      return result;
    }
    return null;
  }

  private generateAccessToken(user: User): string {
    const payload = {
      id: user.id,
      type: 'access'
    };
    return this.jwtService.sign(payload, {
      expiresIn: 600
    });
  }

  private generateRefreshToken(user: User): string {
    const twoWeeks = 60 * 60 * 24 * 14;
    const payload = {
      id: user.id,
      name: user.name,
      email: user.email,
      type: 'refresh'
    };
    return this.jwtService.sign(payload, {
      expiresIn: twoWeeks
    });
  }

  refreshAccessToken(user: any) {
    return {
      accessToken: this.generateAccessToken(user),
    };
  }

  login(user: any) {
    return {
      accessToken: this.generateAccessToken(user),
      refreshToken: this.generateRefreshToken(user)
    };
  }
}