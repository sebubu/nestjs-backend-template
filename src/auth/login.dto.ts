import { Field, ObjectType } from "type-graphql";
import { IsString } from "class-validator";

@ObjectType()
export class LoginDto {
    @Field()
    @IsString()
    accessToken: string;

    @Field({nullable: true})
    @IsString()
    refreshToken: string;
}